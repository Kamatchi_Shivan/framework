import * as React from "react";
import "./css/login.css"
import "./css/font-awesome-4.7.0/css/font-awesome.css"
import "./js/api.js"
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import history from '../../history';
import message from '../messages/message.json'
import { loginAction, logoutAction } from '../../redux/actions/login/loginAction';
import  Captcha  from "./captcha";
import PropTypes from 'prop-types';
import Global from '../layout/Global/Global';
import i18n, { lang, setInitialLocale } from '../../i18n'
import { locale } from "i18n-js";
import { inmemoryLanguage } from "../../redux/actions/language/languageAction";
import { Theme, withStyles, FormControl, InputLabel, Input, InputAdornment, Button, Icon } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

//const { Option } = Select;

// type MyProps = {
//   inmemoryLanguage: any,
//   logoutAction: any,
//   loginAction:any,
//   loginData:any,
//   form: any
// };
// type MyState = {
//   captchaError: string
// };
interface document {
  [key:string]: any;
}

interface ILoginProps {
  //login?: (data: any) => void;
  classes?: any;
  inmemoryLanguage?: any,
  logoutAction?: any,
  loginAction?:any,
  loginData?:any,
  form?: "values"
}

interface ILoginState {
  userName: string;
  password: string;
  showPassword:boolean;
  captchaError:string;
  language: string;
}

const styles = theme => {
  console.log("theme",theme);
  // return ({
  //     paper: {
  //         top: '80px',
  //         boxShadow: theme.shadows[9]
  //     },
  // });
};

class Login extends React.Component<ILoginProps, ILoginState> {

     public state = {
        userName: "",
        password: "",
        showPassword: false,
        captchaError:"",
        language: "en"
     };

     private handleEmailAddressChange = (event: any) => {
      console.log("Email Target VAlue",event)
      this.setState({ userName: event.target.value })
  }

  private handlePasswordChange = (event: any) => {
      this.setState({ password: event.target.value })
  }

  private handleClickShowPassword = () => {
      this.setState({...this.state, showPassword:!this.state.showPassword});
  };

  private handleMouseDownPassword = (event:any) => {
    event.preventDefault();
  };

    // componentWillMount() {
    //     this.props.logoutAction();
    //     this.render();
    //   }

    //   constructor(props:any){
    //     super(props)
    //     this.state = {
    //         captchaError: ''
    //     }
    // }


    handleSelect = (e:any) =>{
      console.log("hanfle select......", e.target.value)      
      localStorage.setItem("language",e.target.value);
      i18n.locale = e.target.value;
      this.setState({language:e.target.value})
      //this.props.inmemoryLanguage(e);
     // setInitialLocale(locale(e));

    };

    private handleLogin = () => {
      console.log("State",this.state)
      console.log("this.propsssssssss",this.props);
        let err;
        let values = this.state;
        if (!err) {
            let recaptcha = (document.forms as any)["myForm"]["g-recaptcha-response"].value;
            if (recaptcha == "") {
              // alert(" Required Captcha");
                //let captchaError=message.captchaErrorMessage;
                let captchaError= i18n.t(lang.error_messages["captcha_error_message"]);
                this.setState({captchaError});
                //this.setState({captchaError:message.captchaErrorMessage});
                console.log("error",captchaError);
                return false;
            }
            else if(recaptcha!=""){
              let captchaError = "";
              this.setState({captchaError});
            }
            console.log('Received values of form: ', values);
            console.log('in loginnnnnn');
            this.props.loginAction(values);
            console.log(values);
        }
      // let values = this.state;
      // this.props.loginAction(values);
  }

    //   handleSubmit = (e:any) => {
    //     console.log("In Handle Submit")
    //     e.preventDefault();
    //     this.props.form.validateFieldsAndScroll((err:any, values:any) => {
    //         if (!err) {
    //             let recaptcha = (document.forms as any)["myForm"]["g-recaptcha-response"].value;
    //             if (recaptcha == "") {
    //               // alert(" Required Captcha");
    //                 //let captchaError=message.captchaErrorMessage;
    //                 let captchaError= i18n.t(lang.error_messages["captcha_error_message"]);
    //                 this.setState({captchaError});
    //                 //this.setState({captchaError:message.captchaErrorMessage});
    //                 console.log("error",captchaError);
    //                 return false;
    //             }
    //             else if(recaptcha!=""){
    //               let captchaError = "";
    //               this.setState({captchaError});
    //             }
    //             console.log('Received values of form: ', values);
    //             console.log('in loginnnnnn');
    //             this.props.loginAction(values);
    //             console.log(values);
    //         }
    //     });
    // };
   

    renderRedirect = () => {
      if (this.props.loginData.redirect) {
          return <Redirect to='/home'/>
      }
    }

    public render(): JSX.Element {
      const classes = this.props.classes;
      //const { getFieldDecorator } = this.props.form;
      return (
          <div className="login-layout">            
           <div className="logo-section">             
              <div className="languagedropdown">                               
              <FormControl variant="outlined">
               <Select defaultValue="en" style={{ width: "100%" }} onChange = {this.handleSelect}>
                  <MenuItem value="en">English</MenuItem>
                  <MenuItem value="fr">French</MenuItem>
                  <MenuItem value="se">Serbian</MenuItem>
                </Select>
              </FormControl>
              </div>
              <img src={require("./images/logo.png")} alt="banner" className="img-responsive" />          
            </div>            
            <div className="login">
              {this.renderRedirect()}
              <div className="login-content">
                <div className="login-top-content">
                  <h1 className="login-title">{i18n.t(lang.labels["welcome_to"])} <span>{i18n.t(lang.labels["expleo"])}</span></h1>
                  <div className="login-text">{i18n.t(lang.labels["on_off_boarding"])}</div>
                  <div className="login-para">{i18n.t(lang.labels["access_application_forever"])}</div>
                  <div className="login-btn">
                    <a href="javascript:void(0)"><span>{i18n.t(lang.buttons["learn_more"])}</span></a>
                  </div>
                </div>
                <img src={require("./images/banner.png")} alt="banner" className="img-responsive" />
              </div>
              <div className="login-form">
                <div className="login-form-section">
                  <h1 className="login-content-title">{i18n.t(lang.labels["login"])}</h1>
                  <p>{i18n.t(lang.labels["login_information"])}</p>
                  <div className="form-group mail-box">
                  <div className="form-info">
                    <FormControl required={true} fullWidth={true}>
                        <InputLabel htmlFor="email">User Name</InputLabel>
                        <Input
                            type="email"
                            value={this.state.userName}
                            onChange={this.handleEmailAddressChange}
                            id="email"
                            startAdornment={
                                <InputAdornment position="start">
                                    <Icon>email</Icon>
                                </InputAdornment>}
                        />
                    </FormControl>
                    </div>
                    </div>
                    <div className="form-group password-box">
                    <div className="form-info">
                    <FormControl required={true} fullWidth={true}>
                        <InputLabel htmlFor="password">Password</InputLabel>
                        <Input
                            value={this.state.password}
                            onChange={this.handlePasswordChange}
                            type={this.state.showPassword ? 'text' : 'password'}
                            id="password"
                            startAdornment={
                                <InputAdornment position="start">
                                    <Icon>lock</Icon>
                                </InputAdornment>}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={this.handleClickShowPassword}
                                  onMouseDown={this.handleMouseDownPassword}
                                >
                                  {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                              </InputAdornment>
                            }
                        />
                    </FormControl>
                    </div>
                    </div>
                    <div className="loginerror">{this.props.loginData.message}</div>
                    <Captcha></Captcha>
                    <div className="captchaerror">{this.state.captchaError}</div>
                    <div className="btn-action">
                      <a href="javascript:void(0);" title="Click to get reset password info" className="login-forget-password">{i18n.t(lang.labels["forgot_password"])}</a>
                        {/* <Button variant="raised" className={classes.button}>
                            Cancel
                        </Button> */}
                        <br></br><br></br>
                        <br></br><br></br>
                        <Button
                            onClick={this.handleLogin}
                            variant="contained"
                            color="primary"
                            className="login-form-btn">
                            {i18n.t(lang.buttons["login_now"])}
                        </Button>
                    </div>
            </div>
                </div>
              </div>
            </div>

      )
  }
}

// const styles = (theme: Theme) => ({
//   container: {
//       display: 'flex',
//       justifyContent: 'center'
//   },
//   paper: theme.mixins.gutters({
//       paddingTop: 16,
//       paddingBottom: 16,
//       marginTop: theme.spacing.unit * 3,
//       width: '30%',
//       display: 'flex',
//       flexDirection: 'column',
//       alignContent: 'center',
//       [theme.breakpoints.down('md')]: {
//           width: '100%',
//       },
//   }),
//   field: {
//       marginTop: theme.spacing.unit * 3
//   },
//   actions: theme.mixins.gutters({
//       paddingTop: 16,
//       paddingBottom: 16,
//       marginTop: theme.spacing.unit * 3,
//       display: 'flex',
//       flexDirection: 'row',
//       alignContent: 'center'
//   }),
//   button: {
//       marginRight: theme.spacing.unit
//   }
// });

const mapStateToProps = (state:any) => ({ loginData: state.loginData });

//const LoginuserForm = Form.create({ name: 'Login' })(Login);

export default connect(mapStateToProps, { loginAction,logoutAction, inmemoryLanguage })(withStyles(styles)(Login));
//export default withStyles(styles, { withTheme: true })(Login as any) as any;