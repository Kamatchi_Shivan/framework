import {combineReducers} from 'redux';
import loginReducer from '../reducers/login/loginReducer';
import languageReducer from "../reducers/language/languageReducer";
import userProfileReducer from "../reducers/profileUpdateReducer/profileUpdateReducer";
import changePasswordReducer from "../reducers/changePasswordReducer/changePasswordReducer";

export default combineReducers({
    languageDatum : languageReducer,
    loginData: loginReducer,
    userProfile: userProfileReducer,
    changePassword: changePasswordReducer
});
