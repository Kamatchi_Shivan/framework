const sonarqubeScanner = require("sonarqube-scanner");

sonarqubeScanner(
    {
      serverUrl: "http://localhost:9000",
      token: "d29f42ea0c39cf7eb3216572ff448000e85329d6",
      options: {
        "sonar.sources": "./components",
        //"sonar.test.inclusions"="**/*.test.js",
        "sonar.exclusions": "__tests__/**",
        //"sonar.tests": "D:/workspace/Framework/expleocore/client/react/src/App.test.tsx",
        //"sonar.test.inclusions": "./src/__tests__/**/*.test.tsx,./src/__tests__/**/*.test.ts,./src/App.test.tsx,D:/workspace/Framework/expleocore/client/react/src/App.test.tsx,./src/App.test.tsx",
        "sonar.typescript.lcov.reportPaths": "coverage/lcov.info",
        "sonar.testExecutionReportPaths": "reports/test-report.xml",
      },
    },
    () => {},
  );