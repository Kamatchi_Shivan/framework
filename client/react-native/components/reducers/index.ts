import { combineReducers } from 'redux';
import authReducer from './auth.reducer';
import timeTableReducer from './timeTable.reducer'
// Redux: Root Reducer
const rootReducer = combineReducers({
  authData: authReducer,
  timeTable: timeTableReducer,
});
// Exports
export default rootReducer;