import React from 'react'
import { Text, View, Dimensions, Image} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { clearData } from '../actions/auth.action';
import styles from './styles'
import { connect } from 'react-redux';
import { FlatGrid } from 'react-native-super-grid';

type MyProps = {
    navigation: any,
    authData: any,
}

const items = [
    { name: 'UI Style Guide', code: '#3498db' },
    { name: 'Authentication', code: '#16a085' },
    { name: 'Push Notification', code: '#9b59b6' },
    { name: 'Logging & Error Handling', code: '#EC6F62' },
    { name: 'SCA Integration', code: '#34495e' },
    { name: 'REO Designer', code: '#7f8c8d' },
    // { name: 'Time Table', code: '#7f8c8d' },
]
class Home extends React.Component<MyProps> {

    navigation: any;
    constructor(props: any) {
        super(props);
        this.navigation = props['navigation'];
    }

    setSelectedCountry(value: any) {
        this.setState({ selectedCountry: value });
    }

    render() {
        return (
            <KeyboardAwareScrollView>
                <View style={{ height: Dimensions.get('screen').height, backgroundColor: 'white' }}>
                    <View style={{ backgroundColor: 'black', height: 50 }}>
                        <View style={{ flexDirection: 'row', flex: 1 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '90%', alignItems: 'center' }}>
                                <Image
                                    source={require('../../assets/splash_icon.png')}
                                    style={{ width: 150, height: 40 }}
                                />

                                <TouchableOpacity style={[{ backgroundColor: 'transparent' }]} onPress={() => this.navigation.navigate('Notifications', { name: 'Jane' })}>
                                    <Image
                                        source={require('../../assets/bell2.png')}
                                        style={{ width: 25, height: 25, alignSelf: 'flex-end', alignItems: 'center' }}
                                    />
                                </TouchableOpacity>
                            </View>
                            {this.props.authData.isLoggedIn ?
                                (<View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-end', alignContent: 'center', alignItems: 'center', marginRight: 10 }}>
                                    <TouchableOpacity style={[{ backgroundColor: 'transparent' }]} onPress={() => this.navigation.navigate('LeftMenu', { name: 'Jane' })}>
                                        <Image
                                            source={require('../../assets/hamburger.png')}
                                            style={{ width: 20, height: 12 }}
                                        />
                                    </TouchableOpacity>
                                </View>) : (undefined)}
                        </View>
                    </View>
                    <FlatGrid
                        itemDimension={130}
                        data={items}
                        style={styles.gridView}
                        spacing={10}
                        renderItem={({ item }) => (
                            <View>
                                {item.name === 'UI Style Guide' ?
                                    <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.navigation.navigate('UiStyle')
                                        }}>
                                            <View style={{ paddingLeft: 60, paddingBottom: 15 }}>
                                                <Image
                                                    source={require('../../assets/guide.png')}
                                                    style={{ width: 40, height: 40 }}
                                                />
                                            </View>
                                            <View style={{ paddingLeft: 35, paddingBottom: 20 }}>
                                                <Text style={styles.itemName}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> : undefined}
                                {item.name === 'Authentication' ?
                                    <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.navigation.navigate('Authentication')
                                        }}>
                                            <View style={{ paddingLeft: 55, paddingBottom: 15 }}>
                                                <Image
                                                    source={require('../../assets/key.png')}
                                                    style={{ width: 50, height: 50 }}
                                                />
                                            </View>
                                            <View style={{ paddingLeft: 50, paddingBottom: 20 }}>
                                                <Text style={styles.itemName}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> : undefined}
                                {item.name === 'Push Notification' ?
                                    <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.navigation.navigate('PushNotification')
                                        }}>
                                            <View style={{ paddingLeft: 60, paddingBottom: 20 }}>
                                                <Image
                                                    source={require('../../assets/bell.png')}
                                                    style={{ width: 40, height: 40 }}
                                                />
                                            </View>
                                            <View style={{ paddingLeft: 38, paddingBottom: 20 }}>
                                                <Text style={styles.itemName}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> : undefined}
                                {item.name === 'Logging & Error Handling' ?
                                    <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.navigation.navigate('ErrorHandling')
                                        }}>
                                            <View style={{ paddingLeft: 60, paddingBottom: 15 }}>
                                                <Image
                                                    source={require('../../assets/error_war.png')}
                                                    style={{ width: 45, height: 40 }}
                                                />
                                            </View>
                                            <View style={{ paddingLeft: 20, paddingBottom: 20 }}>
                                                <Text style={styles.itemName}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> : undefined}
                                {item.name === 'SCA Integration' ?
                                    <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.navigation.navigate('sca')
                                        }}>
                                            <View style={{ paddingLeft: 60, paddingBottom: 15 }}>
                                                <Image
                                                    source={require('../../assets/admin.png')}
                                                    style={{ width: 35, height: 45 }}
                                                />
                                            </View>
                                            <View style={{ paddingLeft: 40, paddingBottom: 20 }}>
                                                <Text style={styles.itemName}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> : undefined}
                                {item.name === 'REO Designer' ?
                                    <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.navigation.navigate('reo')
                                        }}>
                                            <View style={{ paddingLeft: 60, paddingBottom: 15 }}>
                                                <Image
                                                    source={require('../../assets/onboarding.png')}
                                                    style={{ width: 50, height: 50 }}
                                                />
                                            </View>
                                            <View style={{ paddingLeft: 40, paddingBottom: 20 }}>
                                                <Text style={styles.itemName}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> : undefined}
                                    {/* {item.name === 'Time Table' ?
                                    <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                        <TouchableOpacity onPress={() => {
                                            this.navigation.navigate('TimeTable')
                                        }}>
                                            <View style={{ paddingLeft: 60, paddingBottom: 15 }}>
                                                <Image
                                                    source={require('../../assets/onboarding.png')}
                                                    style={{ width: 50, height: 50 }}
                                                />
                                            </View>
                                            <View style={{ paddingLeft: 40, paddingBottom: 20 }}>
                                                <Text style={styles.itemName}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View> : undefined} */}
                            </View>
                        )}
                    />
                </View>
            </KeyboardAwareScrollView>
        )
    }
}


const mapStateToProps = (state: any) => ({
    authData: state.authData
});

function bindToAction(dispatch: any) {
    return {
        clearData: () => dispatch(clearData())
    };
}

export default connect(mapStateToProps, bindToAction)(Home);