import React from 'react'
import { Text, View, Dimensions, Picker, Image as Image1 } from 'react-native'
import Svg, { Image } from 'react-native-svg'
import Animated from 'react-native-reanimated'
import { TouchableOpacity } from 'react-native-gesture-handler'
import commonStyles from '../Common/styles'
import { login, clearData } from '../actions/auth.action'
import { connect } from 'react-redux';
// import { CheckBox } from 'react-native-elements'
import { ActivityIndicator } from 'react-native-paper'
import { TextInput, } from 'react-native-paper'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { strings } from '../../src/locales/i18n';
import RNLanguages from 'react-native-languages';
import i18n from 'i18n-js';
//import { ListPicker} from 'react-native-ultimate-modal-picker';


type MyProps = {
    authData: any,
    login: (userName?: any, password?: any, keepLoggedIn?: any, navigation?: any, isComplete?: any) => void,
    clearData: () => void
}
type MyState = {
    userName: string,
    password: string,
    keepLoggedIn: boolean,
    isComplete: boolean,
    selectedValue: any
}

// const Languages: any = [
//     { label: 'English', value: 'en' },
//     { label: 'Arabic', value: 'ar' },
// ];


const { width, height } = Dimensions.get('screen');
// const { Value, event, block, cond, eq, set, Clock, startClock, stopClock, debug, timing, clockRunning, interpolate, Extrapolate, concat } = Animated;
class Signin extends React.Component<MyProps, MyState> {

    buttonOpacity: any = '';
    textOpacity: any = '';
    onStateChange: any = '';
    onCloseState: any = '';
    buttonY: any = '';
    bgY: any = '';
    textInputZindex: any = '';
    textInputY: any = '';
    textInputOpacity: any = '';
    rotateCross: any = '';
    keyboardDidShowListener: any = '';
    keyboardDidHideListener: any = ''
    navigation: any;
    constructor(props: any) {
        super(props);
        this.navigation = props['navigation'];
        this.state = {
            keepLoggedIn: false,
            isComplete: true,
            userName: '',
            password: '',
            selectedValue: RNLanguages.language
        }
        this.handleUserNameChange = this.handleUserNameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handleUserNameChange(event: any) {
        let processedData = event.nativeEvent.text;
        this.setState({ userName: processedData });
    }

    handlePasswordChange(event: any) {
        let processedData = event.nativeEvent.text;
        this.setState({ password: processedData });
    }
    setSelectedValue(value: any) {
        this.setState({ selectedValue: value });
    }

    render() {
        i18n.locale = this.state.selectedValue;
        i18n.fallbacks = true;
        return (
            <KeyboardAwareScrollView>
                <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'black', height: height }}>
                    <Animated.View style={[commonStyles.container]}>
                        <Svg height={height + 50} width={width}>
                            <Image
                                href={require('../../assets/bg.png')}
                                height={height + 60}
                                width={width}
                                preserveAspectRatio='xMidYMid slice'
                                clipPath='url(#clip)'
                            />
                        </Svg>
                    </Animated.View>
                    {this.state.selectedValue === 'ar' ? (
                        <View style={{ position: 'absolute', top: 0, flexDirection: 'row', width: "50%" }}>
                        <Picker
                            selectedValue={this.state.selectedValue}
                            style={{ height: 25, width: "100%", backgroundColor: "white",flexDirection: 'row-reverse' }}
                            onValueChange={(itemValue, itemIndex) => this.setSelectedValue(itemValue)}
                        >
                            <Picker.Item label="English" value="en" />
                            <Picker.Item label="Arabic" value="ar" />
                            <Picker.Item label="French" value="fr" />
                            <Picker.Item label="Serbian" value="se" />
                        </Picker>
                        {/* <ListPicker
                            title="Language"
                            items={Languages}
                            onChange={(item) =>this.setSelectedValue(item)}
                        /> */}
                        <Text style={{ color: 'white', fontSize: 14, fontWeight: 'bold',flexDirection: 'row-reverse' }}>{strings('login.Lang')} &nbsp;</Text>
                    </View>
                    ):undefined}
                    {this.state.selectedValue !== 'ar' ? (
                        <View style={{ position: 'absolute', top: 0, flexDirection: 'row', alignSelf: 'flex-end', width: "50%" }}>
                        <Text style={{ color: 'white', fontSize: 14, fontWeight: 'bold' }}>{strings('login.Lang')} &nbsp;</Text>
                        <Picker
                            selectedValue={this.state.selectedValue}
                            style={{ height: 25, width: "100%", backgroundColor: "white" }}
                            onValueChange={(itemValue, itemIndex) => this.setSelectedValue(itemValue)}
                        >
                            <Picker.Item label="English" value="en" />
                            <Picker.Item label="Arabic" value="ar" />
                            <Picker.Item label="French" value="fr" />
                            <Picker.Item label="Serbian" value="se" />
                        </Picker>
                        {/* <ListPicker
                            title="Gender"
                            items={Languages}
                            onChange={(item) =>this.setSelectedValue(item)}
                        /> */}
                    </View>
                    ):undefined}
                    
                    <View style={{ position: 'absolute', top: 50, alignSelf: 'center' }}>
                        <Image1
                            source={require('../../assets/splash_icon.png')}
                            style={{ width: 350, height: 150 }}
                        />
                    </View>
                    {this.state.selectedValue === 'ar' ? (
                         <View>                   
                         <Text style={{ color: 'white', fontSize: 26, alignSelf: 'flex-end',paddingRight:25 }}>{strings('login.welcome')}</Text>
                     </View>
                    ):undefined}
                     {this.state.selectedValue !== 'ar' ? (
                          <View>                   
                          <Text style={{ color: 'white', fontSize: 26, alignSelf: 'center' }}>{strings('login.welcome')}</Text>
                      </View>
                     ):undefined}
                    {this.state.selectedValue === 'ar' ? (
                        <View>
                            <TextInput
                                mode="flat"
                                placeholder={strings('login.Email')}
                                style={[commonStyles.textInput, { marginTop: 25, height: 50,flexDirection: 'row-reverse' }]}
                                placeholderTextColor="black"
                                value={this.state.userName}
                                onChange={this.handleUserNameChange}
                            >
                            </TextInput>
                            <TextInput
                                mode="flat"
                                placeholder={strings('login.Password')}
                                secureTextEntry={true}
                                style={[commonStyles.textInput, { height: 50 ,flexDirection: 'row-reverse'}]}
                                placeholderTextColor="black"
                                value={this.state.password}
                                onChange={this.handlePasswordChange}
                            >
                            </TextInput>
                            {this.props.authData.error ? (
                                <View style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                                    {/* <Text style={{ color: 'red', fontSize: 14, fontWeight: 'bold' }}>{this.props.authData.error}</Text> */}
                                    <Text style={{ color: 'red', fontSize: 14, fontWeight: 'bold',marginLeft: 100 }}>{strings('login.Error')}</Text>
                                </View>) : undefined}
                            <Animated.View style={[commonStyles.button1, { backgroundColor: '#6846C6', height: 50 }]}>
                                {this.state.isComplete ? (
                                    <TouchableOpacity style={[{ borderRadius: 5, width: 350 }, commonStyles.button1, { height: 50 }]} onPress={() => {
                                        this.setState({ isComplete: false });
                                        this.props.login(this.state.userName, this.state.password, this.state.keepLoggedIn, () => {
                                            this.navigation.navigate('Home', { name: 'Jane' })
                                        }, () => {
                                            this.setState({ isComplete: true })
                                        });
                                    }}>
                                        <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 15, marginBottom: 15, marginLeft: 250, color: 'white' }}>{strings('login.login_button')}</Text>
                                    </TouchableOpacity>)
                                    : (<ActivityIndicator size="small" color="#00ff00"></ActivityIndicator>)
                                }
                            </Animated.View>
                        </View>
                    ) : undefined}
                    {this.state.selectedValue !== 'ar' ? (
                        <View>
                            <TextInput
                                mode="flat"
                                placeholder={strings('login.Email')}
                                style={[commonStyles.textInput, { marginTop: 25, height: 50 }]}
                                placeholderTextColor="black"
                                value={this.state.userName}
                                onChange={this.handleUserNameChange}
                            >
                            </TextInput>
                            <TextInput
                                mode="flat"
                                placeholder={strings('login.Password')}
                                secureTextEntry={true}
                                style={[commonStyles.textInput, { height: 50 }]}
                                placeholderTextColor="black"
                                value={this.state.password}
                                onChange={this.handlePasswordChange}
                            >
                            </TextInput>
                            {this.props.authData.error ? (
                                <View style={{ marginLeft: 'auto', marginRight: 'auto' }}>
                                    {/* <Text style={{ color: 'red', fontSize: 14, fontWeight: 'bold' }}>{this.props.authData.error}</Text> */}
                                    <Text style={{ color: 'red', fontSize: 14, fontWeight: 'bold' }}>{strings('login.Error')}</Text>
                                </View>) : undefined}
                            <Animated.View style={[commonStyles.button1, { backgroundColor: '#6846C6', height: 50 }]}>
                                {this.state.isComplete ? (
                                    <TouchableOpacity style={[{ borderRadius: 5, width: 350 }, commonStyles.button1, { height: 50 }]} onPress={() => {
                                        this.setState({ isComplete: false });
                                        this.props.login(this.state.userName, this.state.password, this.state.keepLoggedIn, () => {
                                            this.navigation.navigate('Home', { name: 'Jane' })
                                        }, () => {
                                            this.setState({ isComplete: true })
                                        });
                                    }}>
                                        <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 15, marginBottom: 15, marginLeft: 20, marginRight: 20, color: 'white' }}>{strings('login.login_button')}</Text>
                                    </TouchableOpacity>)
                                    : (<ActivityIndicator size="small" color="#00ff00"></ActivityIndicator>)
                                }
                            </Animated.View>
                        </View>
                    ) : undefined}

                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = (state: any) => ({
    authData: state.authData
});

function bindToAction(dispatch: any) {
    return {
        login: (userName?: string, password?: string, keepLoggedIn?: string, navigation?: any, isComplete?: any) =>
            dispatch(login(userName, password, keepLoggedIn, navigation, isComplete)),
        clearData: () => dispatch(clearData())
    };
}

export default connect(
    mapStateToProps,
    bindToAction
)(Signin);
