import React from 'react'
import { Text, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'

class SCA extends React.Component {
    render() {
        return (
            <View >
                <KeyboardAwareScrollView>
                    <View style={{ marginTop: 'auto' }}>
                        <Text style={[styles.Title,{ color: "#6846C7" }]}>Sonar Qube Integration</Text>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>SonarQube Integration is an open source static code analysis tool that is gaining tremendous popularity among software developers. SonarQube enables developers to track code quality, which helps them to ascertain if a project is ready to be deployed in production</Text>
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>With continuous Code Quality SonarQube will enhance your workflow through automated code review, CI/CD integration, pull requests decorations</Text>
                        </View>
                        {/* <View style={{ paddingTop: 10,flexDirection: 'row' }}>
                            <Text
                                style={{ color: 'blue',fontSize: 16, marginHorizontal: 10,lineHeight: 30, }}
                                onPress={() => { Linking.openURL('http://localhost:9000/') }}
                            >Click here </Text>
                            <Text
                                style={{fontSize: 16, marginHorizontal: 10,lineHeight: 30,paddingRight:200}}
                            >to view Sonarqube output </Text>
                        </View> */}
                        <Text style={[styles.Title]}>What we provide</Text>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>For every PR raised, we will be running the sonar qube scanner and based on the output Reviewer will merge the code thus ensuring code quality and all security threats at code level will be taken care.</Text>
                        </View>
                        <Text style={[styles.Title]}>LINT Configuration</Text>
                    </View>
                    <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Linting is the process of running your code through a tool to analyse for potential errors. Linting isn’t language specific and can be customised on a per project basis to ensure code quality, improved performance, reduce developer decision making fatigue and consistency.</Text>
                        </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

export default SCA;