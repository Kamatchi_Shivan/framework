import React, { Component } from 'react'
import DataList from './dataList' // Importing Component Here 
import {connect} from 'react-redux'
import { getTimetableList} from '../actions/timeTable.action' // Calling Redux action
import {config} from '../../config/config'

type MyProps = {
    getTimetableList: any,
    timeTable: any
}

 class TimeTable extends Component<MyProps> {
     componentWillMount(){
         this.props.getTimetableList(config.getTimetableList) 

     }
    render() {
        return (
            <DataList value = {this.props.timeTable}></DataList> // Parent component Passing Values in props here  // Passing Values fro redux
        )
    }
}

const mapStateToProps = (state: any) => ({
    timeTable: state.timeTable
});
  
  
export default connect( mapStateToProps,{getTimetableList} )(TimeTable);