import React from 'react'
import { Text, View, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'

class Authentication extends React.Component {

    render() {
        return (
            <View >
                <KeyboardAwareScrollView>
                    <View style={{ marginTop: 'auto' }}>
                        <Text style={[styles.Title,{ color: "#6846C7" }]}>Authentication</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Authentication is important as it enables organizations to keep their networks secure by permitting only authenticated users (or processes) to access its protected resources, which may include computer systems, networks, databases, websites, and other network-based applications or services.</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Authentication features like fingerprint,facial recognition,PIN,Pattern..etc will be added in future.</Text>
                        </View>
                        <Text style={[styles.Title]}>What we provide</Text>
                        <View style={{paddingTop:10}}>
                        <Image
                            source={require('../../assets/authentication.png')}
                            style={{ width: '90%', height: 90 ,marginHorizontal: 10,alignSelf:'center'}}
                        />
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>In Framework, Authentication / Authorization is enabled as a re-usable functionality implemented with best practices following SOLID principles. Authentication is enabled using DB based username password validation, LDAP lookup for validating a user, and ADFS based authentication. Once the user validated JWT token will be generated using a private key which will prevent security attacks.</Text>
                        </View>
                        </View>
                        <Text style={[styles.Title]}>How we implement</Text>
                        <View style={{paddingTop:10}}>
                       
                        <Text style={[styles.Content]}>1.Configuration engine will look database for the type of authentication user/project configured to.</Text>
                
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}>2.Based on the configuration, it will orchestrate towards corresponding flow to authenticate the user.</Text>
                        </View>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}>3.Once the user is successfully authenticated, user data will be used to generate JWT token based on a secured way of encryption and signing to prevent an attack.</Text>
                        </View>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}>4.Later the JWT token will be used for authorization in all API requests.</Text>
                        </View>
                        </View>            
                        <Text style={[styles.Title]}>How to use</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}> 1.To Use the Authentication module, we have to first go to the database based on what database we choose (MySql / MongoDB).</Text>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}> 2.We need to configure the type of authentication in the database</Text>
                        </View>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}> 3.Based on the type of authentication chosen, we need to provide the necessary configuration parameters. E.g.) In case of LDAP domain URL, username and password to authenticate, bind dn, search base…etc.</Text>
                        </View>
                        </View>
                        <Text style={[styles.Title]}>What to test</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Since the framework is tested already, when we consume the authentication module, we only need to test for configuration and appropriate URLs for DB / LDAP / ADFS.</Text>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}


export default Authentication;