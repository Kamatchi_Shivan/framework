import React from 'react'
import { View, Text,Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles'

type MyProps = {
    value: any,
}

class DataList extends React.Component<MyProps> {  //Dynmaic Component act depends on Data
    generateColumnAndValues(data: any){ // Dynamic Column and Value Redering Function
       let res = [];
       Object.keys(data).forEach(function (key) {
        if(key !== 'IMAGE'){
            res.push( <Text style={{ margin: 5 }}><Text style={{ fontWeight: 'bold' }}>{key} : </Text>{data[key]}</Text>)
        }
    });
    return res;
    }
    generateImages(data: any){ // Dynamic Column and Value Redering Function
        let res = [];
        Object.keys(data).forEach(function (key) {
         if(key === 'IMAGE'){
             res.push( <Image
                source={{
                    uri:
                      data[key],
                  }}
                style={{ width: 120, height: 150 }}
            ></Image>)
         }
     });
     return res;
     }

    render() {
        let data: any = undefined;
        if (this.props.value !== undefined && this.props.value.data && this.props.value.data.length > 0) {
            data = this.props.value.data.slice(0).map((request: any) => {
                return (<View style={{ marginHorizontal: 30, marginVertical: 20, backgroundColor: '#F2F2F2', borderColor: '#d4d4d4', borderWidth: 1, borderRadius: 5}}>
                     <View style={{flexDirection: 'row'}}>
                    <View>
                        {this.generateImages(request)}
                    </View>
                    <View>
                    {this.generateColumnAndValues(request)}
                    <Text style={{ margin: 5,color:"blue" }}>Register Here</Text>
                    </View>
                    </View>
                    
                </View>) // Returing Dynamic column and Values Here 
            });
        } else {
            data = (<View style={{ marginHorizontal: 30, marginVertical: 20, backgroundColor: '#F2F2F2', borderColor: '#d4d4d4', borderWidth: 1, borderRadius: 5, marginTop: 50 }}>

                <Text style={{ margin: 5 }}><Text style={{ fontWeight: 'bold' }}>Column1 : </Text></Text>
                <Text style={{ margin: 5 }}><Text style={{ fontWeight: 'bold' }}>Column2 : </Text> </Text>
                <Text style={{ margin: 5 }}><Text style={{ fontWeight: 'bold' }}>Column3 : </Text> </Text>
            </View>);
        }

        return (
            <KeyboardAwareScrollView style={{ backgroundColor: 'black' }}>
                <View>  
                  <View style={{ backgroundColor: 'black', height: 50 }}>
                            <View style={{ alignSelf:'center',marginTop:10 }}>
                                <Text style={[styles.NotificationText]}>Data List</Text>
                            </View>
                        
                        </View>              
                      <View
                            style={{
                                marginTop:6,
                                borderBottomColor: 'gray',
                                borderBottomWidth: 1,
                            }}
                        />
                    <View>
                    {data}
                    </View>                  
                </View>
            </KeyboardAwareScrollView>
        )
    }
}


export default DataList;