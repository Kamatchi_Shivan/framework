import React from 'react'
import { Text, View, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'

class REO extends React.Component {
    render() {
        return (
            <View >
                <KeyboardAwareScrollView>
                    <View style={{ marginTop: 'auto' }}>
                        <Text style={[styles.Title,{ color: "#6846C7" }]}>Real-time Enterprise Orchestration (REO) Designer</Text>
                        <View style={{
                            paddingTop: 10, flex: 1,
                            alignItems: 'stretch'
                        }}>
                            <Image
                                source={require('../../assets/REO_Designer.png')}
                                style={{ width: 400, height: 200, flex: 1,alignSelf:"center" }}
                            />
                        </View>
                        <View style={{ paddingTop: 20 }}>

                            <Text style={[styles.Content1]}>REO is an orchestration tool built on top of NodeJS and ExpressJS. The features of the designer are as follows</Text>

                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>1.Rapid API development and management.</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>2.Create complex orchestrated APIs in matter of minutes</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>3.By nature all APIs are asynchronous and better performing (NodeJS).</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>4.Highly scalable at any level. You can individually scale an API or group of APIs (modules).</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>5.Ease of use for developers and availability of reusable components.</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>6.Ability to custom components to library and publish it for others to consume.</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>7.Co-existance of API version and very less resource consumption.</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>8.Supports all cutting edge deployment frameworks and platforms.</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}>9.Using DevOps pipeline staging and deployment is quick and 100% automated.</Text>
                            </View>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}


export default REO;