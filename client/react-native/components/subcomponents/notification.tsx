import React from 'react'
import { View, Text } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './styles'
import WS  from 'react-native-websocket'
import {config} from '../../config/config';

type MyProps = {
    navigation: any,
}
type MyState = {
    notifications: any
}
class Notification extends React.Component<MyProps, MyState> {

    navigation: any;
    constructor(props: any) {
        super(props);
        this.navigation = props['navigation'];
        this.state = {
            notifications: [
                {
                    category: 'General',
                    type: 'INFO',
                    message: 'Please keep your credentials safe, do not share it with anyone'
                }
            ]
        }
    }

    handleData(data:any) {
        let result = data.data;
        result = JSON.parse(result)
        let notifications = [result, ...this.state.notifications];
        this.setState({notifications});
      }

    render() {
        let data: any = undefined;
        if (this.state.notifications.length > 0) {
            data = this.state.notifications.slice(0).map((request: any) => {
                return (<View>
                    {request.type==='ERROR' ?(
                        <View  style={{ marginTop:20,marginHorizontal: 10, marginVertical: 5, backgroundColor: '#F2F2F2', borderColor: 'red', borderWidth: 1, borderRadius: 5 }}>
                         <Text style={[styles.alertHeader,{color:"red"}]}>{request.category}</Text>
                         <Text style={[styles.alertcontent,{color:"red"}]}>{request.message}</Text>
                        </View>
                    ):undefined}
                     {request.type==='WARN' ?(
                        <View  style={{ marginTop:20,marginHorizontal: 10, marginVertical: 5, backgroundColor: '#F2F2F2', borderColor: '#FFCC00', borderWidth: 1, borderRadius: 5 }}>
                         <Text style={[styles.alertHeader,{color:"#FFCC00"}]}>{request.category}</Text>
                         <Text style={[styles.alertcontent,{color:"#FFCC00"}]}>{request.message}</Text>
                        </View>
                    ):undefined}
                     {request.type==='INFO' ?(
                        <View  style={{ marginTop:20,marginHorizontal: 10, marginVertical: 5, backgroundColor: '#F2F2F2', borderColor: '#d4d4d4', borderWidth: 1, borderRadius: 5 }}>
                         <Text style={[styles.alertHeader]}>{request.category}</Text>
                         <Text style={[styles.alertcontent]}>{request.message}</Text>
                        </View>
                    ):undefined}
                   
                </View>)
            });
        } else {
            data = (<Text style={{ margin: 20 }}><Text style={{ fontWeight: 'bold', color: 'red' }}>Empty notifications!</Text></Text>);
        }

        return (
            <KeyboardAwareScrollView style={{ backgroundColor: 'white' }}>
                <View>
                    <View style={{ backgroundColor: 'white', height: 50 }}>
                            <View style={{ alignSelf:'center',marginTop:10 }}>
                                <Text style={[styles.NotificationText]}>Notifications</Text>
                        
                        </View>
                        <View
                            style={{
                                marginTop:6,
                                borderBottomColor: 'gray',
                                borderBottomWidth: 1,
                            }}
                        />
                    </View>
                </View>
                <View>
                    {data}
                </View>
                <WS url={config.websocketURL} onMessage={this.handleData.bind(this)}/>
            </KeyboardAwareScrollView>
        )
    }
}


export default Notification;