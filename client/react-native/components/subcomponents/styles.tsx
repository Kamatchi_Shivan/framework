import { StyleSheet, Dimensions } from "react-native";

const {width,height} = Dimensions.get('window');
const styles = StyleSheet.create({ 
    closeButton: {
        height: 40,
        width: 40,
        backgroundColor: 'white',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: -20,
        left: width / 2 - 20,
        shadowOffset: { width: 2, height: 2},
        shadowColor: 'black',
        shadowOpacity: 0.8
    },
    Title:{
      fontSize:20,
      paddingTop:20,
      fontWeight: "bold",
      marginHorizontal: 10
    },
    TitleSmall:{
      fontSize:18,
      paddingTop:10,
      marginHorizontal: 20
    },
    boxContainer:{
      backgroundColor:'black',
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      height:300,
      margin: 20,
      alignItems: 'center',
      justifyContent: 'center'
  },
    Content:{
      fontSize: 16, marginHorizontal: 20,lineHeight: 30,
    },
    LinkContent:{
      fontSize: 16, marginHorizontal: 20,lineHeight: 30, color:"blue"
    },
    Content1:{
      fontSize: 16,lineHeight: 30,marginHorizontal: 10
    },
    NotificationText:{
      fontSize:18,
      paddingTop:10,
      color:'gray'
    },
    alertcontent:{
      margin: 5 ,
      fontSize: 14,
      marginHorizontal: 20,
      lineHeight: 30
    },
    alertHeader:{
      margin: 5,
      alignSelf:'center',
      fontWeight: "bold",
      fontSize: 16,
      color:"gray"
    },
    guidetext:{
      color:'white',
      fontSize:16,
      alignSelf:"center",
      padding:10
    },
    boxStyle:{ 
      flex: 0.3,
      height:40
    },
    boxStyleContainer:{
      flexDirection: "row",
      height: 50,
      padding: 10,
      marginHorizontal: 10,
    }
  });

export default styles;