import React from 'react'
import { Text, View, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'


class Notification extends React.Component {

    render() {
        return (
                <KeyboardAwareScrollView>
                    <View style={{ marginTop: 'auto' }}>
                        <Text style={[styles.Title,{ color: "#6846C7" }]}>Push Notification</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>A push notification is an automated message that is sent to the user by the server of the app that’s working in the background (i.e., not open.) it is a message that’s displayed outside of the app.</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>The Push Notification Framework updates the user interface with real time changes that have occurred on the server tier. The framework enables the PeopleSoft Internet Architecture to push data directly to the end user's display. ... Web Socket push technology on the web server.</Text>
                        </View>
                        <Text style={[styles.Title]}>What we provide</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Push notification for web application. Push notification service will broadcast the message based on the user subscription. If the user does not subscribe for the notification they will not receive notification from push notification service.</Text>
                        </View>
                        <View style={{paddingTop:30,alignItems: 'center', }}>
                        <Image
                            source={require('../../assets/pushNotification.png')}
                            // style={{ width: 410, height: 250,}}
                            style={{ width: '80%', height: 200 ,marginHorizontal: 10,alignSelf:'center'}}
                        />
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}> 1.It is highly scalable and highly secured.</Text>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}> 2.Application agnostic.</Text>
                        </View>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}> 3.Push notification can accept services from multiple applications.</Text>
                        </View>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}> 4.Push notification can receive subscription from multiple applications concurrently.</Text>
                        </View>
                        </View>
                        </View>
                        <Text style={[styles.Title]}>How we implement</Text>
                        <View style={{paddingTop:10}}>
                       
                        <Text style={[styles.Content]}>1.Web socket is used for sending push notification from Server (Push notification service).</Text>
                
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}>2.Subscription details are stored in the database for individual users.</Text>
                        </View>
                        </View>            
                        <Text style={[styles.Title]}>How to use</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}> 1.Push notification service is exposed as a micro service.</Text>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}> 2.If someone wants to push notification to their clients. They can invoke the micro service to get it done.</Text>
                        </View>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.Content]}> 3.If someone wants to manage subscription to push notification, they can modify the configuration in the database</Text>
                        </View>
                        </View>
                        <Text style={[styles.Title]}>What to test</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>User subscription based on the project needs to be tested.</Text>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
        )
    }
}

export default  Notification;