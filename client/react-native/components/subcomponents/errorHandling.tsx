import React from 'react'
import { Text, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'

class ErrorHandling extends React.Component {
    render() {
        return (
            <View >
                <KeyboardAwareScrollView>
                    <View style={{ marginTop: 'auto' }}>
                        <Text style={[styles.Title,{ color: "#6846C7" }]}>Logging & Error Handling</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Logging is an essential component of any application, especially when a critical error occurs. Whether or not you recover from an exception, logging the problem can help you identify the cause of – and ultimately the solution to – potential problems in your application. Logging close to the source of the error provides as much detail about the error as possible.</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Error handling refers to the anticipation, detection, and resolution of programming, application, and communications errors. ... Logic errors, also called bugs, occur when executed code does not produce the expected or desired result. Logic errors are best handled by meticulous program debugging.</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Error handling is important because it makes it easier for the end users of your code to use it correctly. ... If you don't handle your errors, your program may crash, lose all of your customer’s work and you likely won't know where the bug occurred</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Syntax errors, which are typographical mistakes or improper use of special characters, are handled by rigorous proofreading. Logic errors, also called bugs, occur when executed code does not produce the expected or desired result.Logic errors are best handled by meticulous program debugging.</Text>
                        </View>
                        <Text style={[styles.Title]}>What we provide</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>For Error handling, we provide generalized and standard way of handling errors in both client and server tier.</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>In backend, all the errors will be reffered as key in the flow and using generalized implementation, we will look into database / in memory database to fetch error message, user message, error severity based on key and language code.In front end, all the errors will be retrieved from common json file.</Text>
                        </View>
                        <Text style={[styles.Title]}>How we implement</Text>
                        <View style={{paddingTop:3}}>
                       
                        <Text style={[styles.TitleSmall]}>Logging:</Text>
                
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Front End: We have used log4js for logging the error, info and warning in the file and can be extended to log the same information in the database with basic information like timestamp, request details and few other details.</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Back End:In designer, we have extended the logger node to make it as configurable for user to choose logger level (INFO, DEBUG, WARN) and also they can choose to log at File, Database or console wherever applicable in the flow.</Text>
                        </View>
                        </View>            
                        <Text style={[styles.Title]}>How to use</Text>
                        <View style={{paddingTop:3}}>
                        <Text style={[styles.TitleSmall]}> Logging:</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Developer can drag and drop logger node from pallete and inject in flow and choose options best suits for the need.</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.TitleSmall]}> Error Handling:</Text>
                        </View>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Developer can configure the error key in the database if it is not available already based on the language code and use Error hanlding sub flow in the flow and pass error key as the input along with the language code.</Text>
                        </View>
                        </View>
                        <Text style={[styles.Title]}>What to test</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Developer can only test the error configuration in database and logging configuration they have done in the flow rest will be covered as part of the framework.</Text>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

export default ErrorHandling;