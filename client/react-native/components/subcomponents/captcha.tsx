import React from 'react'
import { Text, View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from './styles'
class Captcha extends React.Component{
    render() {
        return (
            <View >
                <KeyboardAwareScrollView>
                    <View style={{ marginTop: 'auto' }}>
                        <Text style={[styles.Title,{ color: "#6846C7" }]}>Captcha Implementation</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Completely Automated Public Turing Tests to Tell Computers and Humans Apart (CAPTCHAs) are still one of the most efficient ways to prevent bots from spamming your website.</Text>
                        </View>
                        <Text style={[styles.Title]}>What we provide</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>As part of framework Captcha validation is support in the login process to prevent bot attacks. Framework configured in a way to choose multiple captcha platform available within the framework.</Text>
                        </View>                       
                        <Text style={[styles.Title]}>How we implement</Text>
                        <View style={{paddingTop:10}}>                       
                        <Text style={[styles.Content]}>Captcha rendering is made as a configuration based rendering in the login UI. If captcha platform required is not available in the platform then it is implemented in the framework and configured to render.</Text>             
                       
                        </View>            
                        <Text style={[styles.Title]}>How to use</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}> You can configure captcha platform you choose to use in the configuration file like (GOOGLE, YAHOO …etc) based on which framework will render the UI.</Text>
                       
                        </View>
                        <Text style={[styles.Title]}>What to test</Text>
                        <View style={{paddingTop:10}}>
                        <Text style={[styles.Content]}>Since captcha platform is already implemented in framework and we are just configuring which platform to use in rendering. We will not need to do any testing specific to this module.</Text>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        )
    }
}

export default Captcha;