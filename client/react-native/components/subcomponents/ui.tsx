import React from 'react'
import { Text, View, Linking, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { TouchableOpacity } from 'react-native-gesture-handler'
import styles from './styles'

class UiStyle extends React.Component {
    render() {
        return (
                <KeyboardAwareScrollView>
                    <View style={{ marginTop: 'auto' }}>
                        <Text style={[styles.Title, { color: "#6846C7" }]}>Introduction of Expleo Style guide</Text>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>A style guide, when well followed, will</Text>
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}> 1.Set the standard for UI quality across the platform.</Text>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}> 2.Promote consistency across website/mobile UI</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}> 3.Give users a feeling of familiarity across the website/mobile.</Text>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.Content]}> 4.Increase conversions owing to great UI/UX.</Text>
                            </View>
                            <View style={{ paddingTop: 10 }}>
                                <Text style={[styles.Content]}>Style Guides should be learned, understood, and implemented at all times on a project which is governed by one, and any deviation must be justified. A style guide (or manual of style) is a set of standards for the writing and design of documents, either for general use or for a specific publication, organization, or field.</Text>
                            </View>
                        </View>
                        <Text style={[styles.Title, { color: "#6846C7" }]}>Brand Colors</Text>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>The core brand colors carry the most brand recognition. Each color was carefully selected to express the personality of Expleo</Text>
                        </View>
                        <View style={{ paddingTop: 3 }}>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}> Primary Colors:</Text>
                            <View style={{ paddingTop: 10 }}>
                                <Text style={[styles.Content]}>The primary palette consists of 1 warm tone, 7 cool tones and 3 neutral tones. When using a primary color as a core, both primary and secondary colors can be used to complement.</Text>
                            </View>
                            <View style={{ paddingTop: 10 }}>
                                <Image
                                    source={require('../../assets/primary.png')}
                                    style={{ width: '90%', height: 40, marginHorizontal: 10, alignSelf: 'center' }}
                                />
                            </View>
                             {/* <View style={[styles.boxStyleContainer]}>
                            <View style={[styles.boxStyle,{ backgroundColor: "#6846C7"}]}><Text style={[styles.guidetext,{fontSize:9}]}>#6846C7</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "#805CE5"}]}><Text style={[styles.guidetext,{fontSize:9}]}>#805CE5</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "#0C0814"}]}><Text style={[styles.guidetext,{fontSize:9}]}>#0C0814</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "#1C1B25"}]}><Text style={[styles.guidetext,{fontSize:9}]}>#1C1B25</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "F0F0F0"}]}><Text style={[styles.guidetext,{color:'black',fontSize:9}]}>#F0F0F0</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "E2E2E2"}]}><Text style={[styles.guidetext,{color:'black',fontSize:9}]}>#E2E2E2</Text></View>
                        </View> */}
                        </View>
                        <View style={{ paddingTop: 3 }}>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}> Secondary Colors:</Text>
                            <View style={{ paddingTop: 10 }}>
                                <Text style={[styles.Content]}>The Secondary palette consists of 1 warm tone, 7 cool tones and 3 neutral tones. When using a primary color as a core, both primary and secondary colors can be used to complement.</Text>
                            </View>
                            <View style={[styles.boxStyleContainer]}>
                            <View style={[styles.boxStyle,{ backgroundColor: "#23B3D9"}]}><Text style={[styles.guidetext,{fontSize:14}]}>#23B3D9</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "#4BCCB9"}]}><Text style={[styles.guidetext,{fontSize:14}]}>#4BCCB9</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "#BC22BF"}]}><Text style={[styles.guidetext,{fontSize:14}]}>#BC22BF</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "#EC6F62"}]}><Text style={[styles.guidetext,{fontSize:14}]}>#EC6F62</Text></View>
                        </View>
                        </View>
                        <View style={{ paddingTop: 3 }}>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}> Text Colors:</Text>
                            <View style={{ paddingTop: 10 }}>
                                <Text style={[styles.Content]}>Readable text colors affects how users process the information in the content. Good contrasts will make text easy on the eyes, easy to scan quickly, and overall more readable.</Text>
                            </View>
                            {/* <View style={{ paddingTop: 10 }}>
                                <Image
                                    source={require('../../assets/text.png')}
                                    style={{ width: '40%', height: 40, marginHorizontal: 20 }}
                                />
                            </View> */}
                            <View style={[styles.boxStyleContainer]}>
                            <View style={[styles.boxStyle,{ backgroundColor: "black"}]}><Text style={[styles.guidetext]}>#0C0814</Text></View>
                            <View style={[styles.boxStyle,{ backgroundColor: "white"}]}><Text style={[styles.guidetext,{color:'black'}]}>#FFFFFF</Text></View>
                        </View>
                        </View>
                        <View style={{ paddingTop: 3 }}>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}> Heading Colors:</Text>
                        </View>
                        <View style={[styles.boxStyleContainer]}>
                            <View style={[styles.boxStyle,{ backgroundColor: "#6846C7",}]}><Text style={[styles.guidetext]}>#6846C7</Text></View>
                        </View>
                        <View style={{ paddingTop: 3 }}>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}> Link Colors:</Text>
                        </View>
                        <View style={[styles.boxStyleContainer]}>
                            <View style={[styles.boxStyle,{ backgroundColor: "#6846C7",}]}><Text style={[styles.guidetext]}>#6846C7</Text></View>
                        </View>
                        <Text style={[styles.Title, { color: "#6846C7" }]}>Images and Icons(Material Icons & Custom SVG Icons)</Text>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>Illustrations and icons improve user comprehension and inject friendliness and humanity to the overall experience.</Text>
                        </View>
                        <Text style={[styles.Title, { color: "#6846C7" }]}>Image Optimization</Text>
                        <View style={{ paddingTop: 3 }}>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}>Optimization-Level1:</Text>
                            <View style={{ paddingTop: 10 }}>
                                <Text style={[styles.Content]}> 1.Consider jpg format. Suppose if need transparency, then only consider png(Only png8).</Text>
                                <View style={{ paddingTop: 3 }}>
                                    <Text style={[styles.Content]}> 2.Use Web Mode whenever saving jpg/png.</Text>
                                </View>
                                <View style={{ paddingTop: 3 }}>
                                    <Text style={[styles.Content]}> 3.Change quality dropdown value(Medium,low,High) for image quality, Whenever saving. Prefer Medium quality.</Text>
                                </View>
                            </View>
                            <View style={{ paddingTop: 3 }}>
                                <Text style={[styles.TitleSmall]}>Optimization-Level2:</Text>
                                <View style={{ paddingTop: 10 }}>
                                    <Text style={[styles.Content]}> Use below any url for next level optimization</Text>
                                    <View style={{ paddingTop: 3 }}>
                                        <TouchableOpacity onPress={() => Linking.openURL('https://kraken.io/web-interface')}>
                                            <Text style={[styles.LinkContent]}> https://kraken.io/web-interface</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ paddingTop: 3 }}>
                                        <TouchableOpacity onPress={() => Linking.openURL('https://tinypng.com/')}>
                                            <Text style={[styles.LinkContent]}> https://tinypng.com/</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}>GIF Optimization:</Text>
                            <View style={{ paddingTop: 10 }}>
                                <Text style={[styles.Content]}> 1.Consider less number of layers.</Text>
                                <View style={{ paddingTop: 3 }}>
                                    <Text style={[styles.Content]}> 2.Consider less number of layers..</Text>
                                </View>
                                <View style={{ paddingTop: 3 }}>
                                    <Text style={[styles.Content]}> 3.To avoid patches, Matte color should apply while saving.</Text>
                                </View>
                            </View>
                        </View>
                        {/* <Text style={[styles.Title, { color: "#6846C7" }]}>Button</Text>
                        <View style={{ paddingTop: 3 }}>
                            <Text style={[styles.TitleSmall, { color: "#6846C7" }]}>Mobile Sticky Button Size:</Text>
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>Persistent footer buttons with flat colors and are used in screen footers to establish a consistent experience.</Text>
                        </View> */}
                        {/* <View style={{paddingTop:10}}>
                            <Image
                            source={require('../../assets/Button.png')}
                            style={{ width: '85%', height: 50 ,marginHorizontal: 20}}
                            />
                            </View> */}
                        <Text style={[styles.Title, { color: "#6846C7" }]}>Resolutions</Text>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>The number of points of color or pixels contained on a display monitor, expressed in terms of the number of pixels on the horizontal axis and the number on the vertical axis is known as resolution.</Text>
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Image
                                source={require('../../assets/mobile.png')}
                                style={{ width: '30%', height: 200, marginHorizontal: 20 }}
                            />
                        </View>
                        <Text style={[styles.Title, { color: "#6846C7" }]}>Source Files</Text>
                        <View style={{ paddingTop: 10 }}>
                            <Text style={[styles.Content]}>Kindly refer below url for source guidelines.</Text>
                        </View>
                        <View style={{ paddingTop: 3 }}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://expleogroup.com/about-us/')}>
                                <Text style={[styles.LinkContent]}>https://expleogroup.com/</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
        )
    }
}


export default UiStyle;