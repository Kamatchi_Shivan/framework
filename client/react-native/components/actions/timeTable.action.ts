import {CLEAR_DATA, MYREQUEST} from './types';
import { Dispatch } from "redux";
import axios from 'axios'
import {config} from "../../config/config";

export function getTimetableList(dataURl: any, navigation?:any, isComplete?:any,url?:any) {
  return (dispatch: Dispatch) => {
      axios.get(config.apiRootPath+dataURl)
        .then(res => {
          dispatch({
            type: MYREQUEST,
            data: res.data,
          });
        }, err => {
          dispatch({
            type: MYREQUEST,
            data: {
              error: "Internal error, please check after some time",
              token: ''
            },
          });
        });
  };
}

export function clearData(){
  return (dispatch:Dispatch) => {
    dispatch({
      type: CLEAR_DATA,
      data: {},
    });
  }
}
