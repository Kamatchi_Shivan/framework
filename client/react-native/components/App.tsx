import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import Home from './Home/Home'
import LeftNav from './LeftMenu/LeftMenu'
import Signin from './Login/Signin';
import Authentication from './subcomponents/authentication'
import Notification from './subcomponents/pushNotification'
import Captcha from './subcomponents/captcha'
import ErrorHandling from './subcomponents/errorHandling'
import SCA from './subcomponents/sca'
import REO from './subcomponents/reo'
import DataList from './subcomponents/dataList'
import Notifications from './subcomponents/notification'
import TimeTable from './subcomponents/timeTable'
import UiStyle from './subcomponents/ui'

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          gestureEnabled: true,
          ...TransitionPresets.FadeFromBottomAndroid
        }}>
        <Stack.Screen
          name="Signin"
          component={Signin}
          options={{headerShown: false}}
        />
        <Stack.Screen 
          name="Home" 
          component={Home}
          options={{headerShown: false}}/>
           <Stack.Screen
          name="TimeTable"
          component={TimeTable}
          options={{headerShown: false}}
        />
          <Stack.Screen
          name="Authentication"
          component={Authentication}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PushNotification"
          component={Notification}
          options={{headerShown: false}}
        />
          <Stack.Screen
          name="DataList"
          component={DataList}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Captcha"
          component={Captcha}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ErrorHandling"
          component={ErrorHandling}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="sca"
          component={SCA}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="reo"
          component={REO}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="Notifications"
          component={Notifications}
          options={{headerShown: false}}
        />
         <Stack.Screen
          name="UiStyle"
          component={UiStyle}
          options={{headerShown: false}}
        />
        <Stack.Screen 
          name="LeftMenu" 
          component={LeftNav}
          options={{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}