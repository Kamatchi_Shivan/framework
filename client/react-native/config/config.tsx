export const config = {
    companyName: 'Expleo',
    //apiRootPath: "https://expleoconnect.azurewebsites.net",
    apiRootPath1: "https://expleoconnect.azurewebsites.net",
    apiRootPath: "http://10.0.2.2:8000/api",
    authenticationURL:"/v2/authenticate",
    getTimetableList:"/v1/getDataTableList",
    getData:"/v1/getTeacherDetails",
    websocketRootPath:"ws://localhost:8000/",
    websocketURL:"ws://10.0.2.2:8000/api/ws/notification",
};