import React from "react";
import { Nav } from "shards-react";

import SidebarNavItem from "./SidebarNavItem";

type MyProps = {};
type MyState = {
  navItems: Array<
    {
      title: string,
      htmlBefore: string,
      to: string,

    }
  >
};
class SidebarNavItems extends React.Component<MyProps, MyState> {
  constructor(props:any) {
    super(props)

    this.state = {
      navItems: [
        {
          title: "UI/UX Style Guide",
          htmlBefore: '<i class="material-icons">table_chart</i>',
          to: "/uiux",
        },
        {
          title: "Authentication",
          htmlBefore: '<i class="material-icons">vpn_key</i>',
          to: "/home",
        },
        {
          title: "Push Notification",
          htmlBefore: '<i class="material-icons">notifications_active</i>',
          to: "/notification",
        },
        {
          title: "Captcha Implementation",
          htmlBefore: '<i class="material-icons">android</i>',
          to: "/captcha",
        },
        {
          title: "Logging & Error Handling",
          htmlBefore: '<i class="material-icons">new_releases</i>',
          to: "/logging",
        },
        {
          title: "SCA Integration",
          htmlBefore: '<i class="material-icons">admin_panel_settings</i>',
          to: "/sonarqube",
        },
        {
          title: "REO Designer",
          htmlBefore: '<i class="material-icons">architecture</i>',
          to: "/design",
        }
      ]
    };
  }


  render() {
    const { navItems: items } = this.state;
    return (
      <div className="nav-wrapper">
        <Nav className="nav--no-borders flex-column">
          {items.map((item:any, idx:any) => (
            <SidebarNavItem key={idx} item={item} />
          ))}
        </Nav>
      </div>
    )
  }
}

export default SidebarNavItems;
