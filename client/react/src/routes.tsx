import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, LoginLayout, LeftNavLayout } from "./layouts";

// Route Views
import Dashboard from "./views/dashboard/Dashboard";
import LoginApp from "./components/login/login";
import Errors from "./views/errors/Errors";
import ChangePassword from "./views/changepassword/changePassword";
import EditProfile from "./views/editprofile/editProfile";
import CaptchaImplementation from "./views/sidemenus/captchaImplementation";
import Designer from "./views/sidemenus/designer";
import ErrorHandling from "./views/sidemenus/errorHandling";
import PushNotification from "./views/sidemenus/pushNotification";
import SonarIntegration from "./views/sidemenus/sonarIntegration";
import UIUX from './views/sidemenus/uiux'

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/login" />
  },
  {
    path: "/login",
    layout: LoginLayout,
    component: LoginApp
  },
  {
    path: "/home",
    layout: LeftNavLayout,
    component: Dashboard
  },
  {
    path: "/errors",
    layout: LeftNavLayout,
    component: Errors
  },
  {
    path: "/changepassword",
    layout: LeftNavLayout,
    component: ChangePassword
  },
  {
    path: "/editprofile",
    layout: LeftNavLayout,
    component: EditProfile
  },
  {
    path: "/notification",
    layout: LeftNavLayout,
    component: PushNotification
  },
  {
    path: "/captcha",
    layout: LeftNavLayout,
    component: CaptchaImplementation
  },
  {
    path: "/logging",
    layout: LeftNavLayout,
    component: ErrorHandling
  },
  {
    path: "/sonarqube",
    layout: LeftNavLayout,
    component: SonarIntegration
  },
  {
    path: "/design",
    layout: LeftNavLayout,
    component: Designer
  },
  {
    path: "/uiux",
    layout: LeftNavLayout,
    component: UIUX
  }
];
