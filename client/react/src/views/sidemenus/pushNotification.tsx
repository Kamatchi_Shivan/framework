import React, {Component} from "react";
import {connect} from 'react-redux';


class PushNotification extends Component {


    render() {
        return (
            <div className="card">
                <div className="card-header">
                Push Notification
                </div>
                <div className="card-body">
                    <h5 className="card-title">Push Notification</h5>
                    <p className="card-text">A push notification is an automated message that is sent to the user by the server of the app that’s working in the background (i.e., not open.) it is a message that’s displayed outside of the app.</p>
                    <p className="card-text">The Push Notification Framework updates the user interface with real time changes that have occurred on the server tier. The framework enables the PeopleSoft Internet Architecture to push data directly to the end user's display. ... Web Socket push technology on the web server.</p>
                    <h5 className="card-title">What we provide</h5>
                    <p className="card-text">Push notification for web application. Push notification service will broadcast the message based on the user subscription. If the user does not subscribe for the notification they will not receive notification from push notification service.</p>
                    <img src={require("../../assets/images/pushNotification.png")} alt="Push Notification" className="img-responsive-banner" />
                    <h5 className="card-title">How we implement</h5>
                    <ul>
                        <li>Web socket is used for sending push notification from Server (Push notification service).</li>
                        <li>Subscription details are stored in the database for individual users.</li>                        
                    </ul>
                    <h5 className="card-title">How to use</h5>
                    <ul>
                        <li>Push notification service is exposed as a micro service. </li>
                        <li>If someone wants to push notification to their clients. They can invoke the micro service to get it done.</li>
                        <li>If someone wants to manage subscription to push notification, they can modify the configuration in the database</li>
                    </ul>
                    <h5 className="card-title">What to test</h5>
                    <ul>
                        <li>User subscription based on the project needs to be tested.</li>
                    </ul>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(PushNotification);
