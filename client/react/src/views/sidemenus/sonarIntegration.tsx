import React, {Component} from "react";
import {connect} from 'react-redux';


class SonarIntegration extends Component {


    render() {
        return (
            <div className="card">
                <div className="card-header">
                Sonar Qube Integration
                </div>
                <div className="card-body">
                    <h5 className="card-title">Sonar Qube Integration</h5>
                    <p className="card-text">SonarQube Integration is an open source static code analysis tool that is gaining tremendous popularity among software developers. SonarQube enables developers to track code quality, which helps them to ascertain if a project is ready to be deployed in production</p>
                    <p className="card-text">With continuous Code Quality SonarQube will enhance your workflow through automated code review, CI/CD integration, pull requests decorations</p>
                    <p><a href="http://localhost:9000/" target="_blank">Click Here</a> to view Sonarqube output</p>
                    <h5 className="card-title">What we provide </h5>
                    <p className="card-text">For every PR raised, we will be running the sonar qube scanner and based on the output Reviewer will merge the code thus ensuring code quality and all security threats at code level will be taken care.</p>
                    <h5 className="card-title">LINT Configuration</h5>
                    <p className="card-text"></p>                   </div>
            </div>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(SonarIntegration);
