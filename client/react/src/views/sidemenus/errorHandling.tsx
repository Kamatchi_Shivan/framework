import React, {Component} from "react";
import {connect} from 'react-redux';


class ErrorHandling extends Component {


    render() {
        return (
            <div className="card">
                <div className="card-header">
                Logging & Error Handling 
                </div>
                <div className="card-body">
                    <h5 className="card-title">Logging & Error Handling </h5>
                    <p className="card-text">Logging is an essential component of any application, especially when a critical error occurs. Whether or not you recover from an exception, logging the problem can help you identify the cause of – and ultimately the solution to – potential problems in your application. Logging close to the source of the error provides as much detail about the error as possible.</p>
                    <p className="card-text">Error handling refers to the anticipation, detection, and resolution of programming, application, and communications errors. ... Logic errors, also called bugs, occur when executed code does not produce the expected or desired result. Logic errors are best handled by meticulous program debugging.</p>
                    <p className="card-text">Error handling is important because it makes it easier for the end users of your code to use it correctly. ... If you don't handle your errors, your program may crash, lose all of your customer’s work and you likely won't know where the bug occurred </p>
                    <p className="card-text">Syntax errors, which are typographical mistakes or improper use of special characters, are handled by rigorous proofreading. Logic errors, also called bugs, occur when executed code does not produce the expected or desired result.Logic errors are best handled by meticulous program debugging.</p>
                    <h5 className="card-title">What we provide </h5>
                    <p className="card-text">For Error handling, we provide generalized and standard way of handling errors in both client and server tier.</p>
                    <p className="card-text">In backend, all the errors will be reffered as key in the flow and using generalized implementation, we will look into database / in memory database to fetch error message, user message, error severity based on key and language code.In front end, all the errors will be retrieved from common json file.</p>
                    <h5 className="card-title">How we implement</h5>
                    <label>Logging: </label>
                    <p className="card-text">Front End: We have used log4js for logging the error, info and warning in the file and can be extended to log the same information in the database with basic information like timestamp, request details and few other details.</p>
                    <p className="card-text">Back End:In designer, we have extended the logger node to make it as configurable for user to choose logger level (INFO, DEBUG, WARN) and also they can choose to log at File, Database or console wherever applicable in the flow.</p>
                    <h5 className="card-title">How to use</h5>
                    <label>Logging: </label>
                    <p className="card-text">Developer can drag and drop logger node from pallete and inject in flow and choose options best suits for the need.</p>
                    <label>Error Handling: </label>
                    <p className="card-text">Developer can configure the error key in the database if it is not available already based on the language code and use Error hanlding sub flow in the flow and pass error key as the input along with the language code.</p>
                    <h5 className="card-title">What to test</h5>
                    <p className="card-text">Developer can only test the error configuration in database and logging configuration they have done in the flow rest will be covered as part of the framework.</p>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(ErrorHandling);
