import React, {Component} from "react";
import {connect} from 'react-redux';


class CaptchaImplementation extends Component {


    render() {
        return (
            <div className="card">
                <div className="card-header">
                Captcha Implementation
                </div>
                <div className="card-body">
                    <h5 className="card-title">Captcha Implementation</h5>
                    <p className="card-text">Completely Automated Public Turing Tests to Tell Computers and Humans Apart (CAPTCHAs) are still one of the most efficient ways to prevent bots from spamming your website.</p>
                    <h5 className="card-title">What we provide </h5>
                    <p className="card-text">As part of framework Captcha validation is support in the login process to prevent bot attacks. Framework configured in a way to choose multiple captcha platform available within the framework.</p>
                    <h5 className="card-title">How we implement</h5>
                    <p className="card-text">Captcha rendering is made as a configuration based rendering in the login UI. If captcha platform required is not available in the platform then it is implemented in the framework and configured to render.</p>
                    <h5 className="card-title">How to use</h5>
                    <p className="card-text">You can configure captcha platform you choose to use in the configuration file like (GOOGLE, YAHOO …etc) based on which framework will render the UI.</p>
                    <h5 className="card-title">What to test</h5>
                    <p className="card-text">Since captcha platform is already implemented in framework and we are just configuring which platform to use in rendering. We will not need to do any testing specific to this module.</p>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(CaptchaImplementation);
