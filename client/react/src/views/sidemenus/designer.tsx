import React, {Component} from "react";
import {connect} from 'react-redux';


class Designer extends Component {


    render() {
        return (
            <div>
                <h3>Real-time Enterprise Orchestration (REO) Designer</h3>
                <div className="row">
                  <div className="col-md-6">
                    <img src={require("../../assets/images/REO_Designer.png")} alt="REO Designer" className="img-responsive-banner"
                      style={{height: '400px', marginLeft: '20px'}}/>
                  </div>
                  <div className="col-md-5">
                    REO is an orchestration tool built on top of NodeJS and ExpressJS. The features of the designer are as follows
                    <ul>
                      <li style={{marginBottom: '10px', marginTop: '10px'}}>Rapid API development and management</li>
                      <li style={{marginBottom: '10px'}}>Create complex orchestrated APIs in matter of minutes</li>
                      <li style={{marginBottom: '10px'}}>By nature all APIs are asynchronous and better performing (NodeJS)</li>
                      <li style={{marginBottom: '10px'}}>Highly scalable at any level. You can individually scale an API or group of APIs (modules)</li>
                      <li style={{marginBottom: '10px'}}>Ease of use for developers and availability of reusable components</li>
                      <li style={{marginBottom: '10px'}}>Ability to custom components to library and publish it for others to consume</li>
                      <li style={{marginBottom: '10px'}}>Co-existance of API version and very less resource consumption</li>
                      <li style={{marginBottom: '10px'}}>Supports all cutting edge deployment frameworks and platforms</li>
                      <li style={{marginBottom: '10px'}}>Using DevOps pipeline staging and deployment is quick and 100% automated</li>
                    </ul>
                  </div>
                </div>

            </div>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(Designer);
