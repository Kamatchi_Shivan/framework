import React, {Component} from "react";
import {connect} from 'react-redux';

type MyProps = {
    loginData:any    
};

class Dashboard extends Component<MyProps> {

    constructor(props:any) {
        super(props);
        this.state = {
            consversionPanel: true
        }
    }

    componentWillMount() {
        console.log("loginData:>>>>>>>", this.props.loginData);
        console.log("UserName:>>>>>>>", this.props.loginData.username);
      }

    render() {
        return (
            <div className="card">
                <div className="card-header">
                Authentication
                </div>
                <div className="card-body">
                    <h5 className="card-title">Authentication</h5>
                    <p className="card-text">Authentication is important as it enables organizations to keep their networks secure by permitting only authenticated users (or processes) to access its protected resources, which may include computer systems, networks, databases, websites, and other network-based applications or services.</p>                    
                    <h5 className="card-title">What we provide </h5>
                    <img src={require("../../assets/images/authentication.png")} alt="Authentication" className="img-responsive-banner" />
                    <p className="card-text">In Framework, Authentication / Authorization is enabled as a re-usable functionality implemented with best practices following SOLID principles. Authentication is enabled using DB based username password validation, LDAP lookup for validating a user, and ADFS based authentication. Once the user validated JWT token will be generated using a private key which will prevent security attacks</p>
                    <h5 className="card-title">How we implement</h5>
                    <ul>
                        <li>1.Configuration engine will look database for the type of authentication user/project configured to.</li>
                        <li>2.Based on the configuration, it will orchestrate towards corresponding flow to authenticate the user.</li>
                        <li>3.Once the user is successfully authenticated, user data will be used to generate JWT token based on a secured way of encryption and signing to prevent an attack.</li>
                        <li>4.Later the JWT token will be used for authorization in all API requests.</li>
                    </ul>                   
                    <h5 className="card-title">How to use</h5>
                    <ul>
                        <li>1.To Use the Authentication module, we have to first go to the database based on what database we choose (MySql / MongoDB).  </li>
                        <li>2.We need to configure the type of authentication in the database</li>
                        <li>3.Based on the type of authentication chosen, we need to provide the necessary configuration parameters. E.g.) In case of LDAP domain URL, username and password to authenticate, bind dn, search base…etc.</li>                        
                    </ul>
                    <h5 className="card-title">What to test</h5> 
                    <p className="card-text">Since the framework is tested already, when we consume the authentication module, we only need to test for configuration and appropriate URLs for DB / LDAP / ADFS.</p>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state:any) => ({loginData:state.loginData,});
export default connect(mapStateToProps)(Dashboard);
